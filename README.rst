#########
Rangespec
#########

This small library is used to parse and unparse page range specification strings,
 eg. "1-3,4" corresponds to (1,2,3,4).

