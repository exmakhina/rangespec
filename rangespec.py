#!/usr/bin/env python
# -*- coding:utf-8 vi:noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-rangespec@zougloub.eu>
# SPDX-License-Identifier: MIT

import typing as t


def sequence_from_rangespec(spec: str):
	"""
	Parse a page specification string into pages as a generator.
	"""
	current = 0
	start = None
	in_range = False

	for char in spec:
		if char.isdigit():
			current = current * 10 + int(char)
		elif char == '-' and current is not None:
			start = current
			current = 0
			in_range = True
		elif char == ',' or char == '-' or char == spec[-1]:
			if in_range:
				if char != '-' and start is not None:  # End of range
					for page in range(start, current + 1):
						yield page
					start = None
					in_range = False
					current = 0
			else:
				if current != 0 or (char == spec[-1] and char.isdigit()):  # Single page
					yield current
					current = 0
		else:
			raise ValueError(char)

	if current != 0:  # Handle last page if there's no comma at the end
		if in_range and start is not None:
			for page in range(start, current + 1):
				yield page
		else:
			yield current


def rangespec_from_sequence(numbers: t.Sequence[int]):
	"""
	Compress a sorted sequence of integers into a range specification string.
	"""
	if not numbers:
		return ""

	start = numbers[0]
	prev = numbers[0]
	result = []

	for number in numbers[1:]:
		if number == prev + 1:
			prev = number
		else:
			if start == prev:
				result.append(f"{start}")
			else:
				result.append(f"{start}-{prev}")
			start = prev = number

	if start == prev:
		result.append(f"{start}")
	else:
		result.append(f"{start}-{prev}")

	return ",".join(result)


