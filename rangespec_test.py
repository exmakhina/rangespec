#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-rangespec@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging

from .rangespec import *


logger = logging.getLogger(__name__)

def testa():
	spec = "1-3,4,5-6"
	assert tuple(sequence_from_rangespec(spec)) == (1,2,3,4,5,6)

def test1():
	spec = "1"
	assert tuple(sequence_from_rangespec(spec)) == (1,)

def test2():
	spec = "1,3"
	assert tuple(sequence_from_rangespec(spec)) == (1,3)

def test3():
	spec = "1,2"
	assert tuple(sequence_from_rangespec(spec)) == (1,2)

def test4():
	spec = "1-2"
	assert tuple(sequence_from_rangespec(spec)) == (1,2)

def test5():
	spec = "1-3"
	assert tuple(sequence_from_rangespec(spec)) == (1,2,3)

def test_rangespec_from_sequence_0():
	numbers = [1]
	spec = rangespec_from_sequence(numbers)
	assert spec == "1"

def test_rangespec_from_sequence_1():
	numbers = [1,2]
	spec = rangespec_from_sequence(numbers)
	logger.debug("%s -> %s", numbers, spec)
	assert spec in ("1-2", "1,2")

def test_rangespec_from_sequence_2():
	numbers = [1,3]
	spec = rangespec_from_sequence(numbers)
	assert spec == "1,3"

def test_rangespec_from_sequence_3():
	numbers = [1, 2, 3, 4, 5, 6, 10, 11, 12, 15]
	spec = rangespec_from_sequence(numbers)
	assert spec == "1-6,10-12,15"
